#include <iostream>
#include <cassert>
#include <list>

#include "unrolled_list.hpp"

typedef  unrolled_list<int, 4> Unrolled;
//typedef  std::list<int> Unrolled;

const int NUM = 13;
const int ORIGINAL[] = {3, 4, 6, 11, 13, 16, 32, 36, 42, 43, 56, 57, 64};

bool compare_lists(const Unrolled & list_0, const Unrolled & list_1)
{
    bool res = ( list_0.size() == list_1.size() );

    if(!res)
        return false;

    auto it_0 = list_0.begin();
    auto end_0 = list_0.end();
    auto it_1 = list_1.begin();
    auto end_1 = list_1.end();
    while( (it_0 != end_0) && (it_1 != end_1) ) {
        res &= (*it_0 == *it_1);
        ++it_0;
        ++it_1;
    }

    return res;
}

void test_list(const Unrolled & list)
{
    auto it = list.begin();
    auto end = list.end();
    for (int i = 0; i < NUM; ++i) {
        assert(*it == ORIGINAL[i]);
        assert(it != end);
        ++it;
    }

    assert(it == end);

    std::cout << "test \'test_list\'...  passed\n";
    std::cout.flush();
}

void test_len(const Unrolled & list)
{
    assert(list.size() == NUM);

    std::cout << "test \'test_len\'...  passed\n";
    std::cout.flush();
}

void test_iter(const Unrolled & list)
{
    auto it = list.begin();
    auto end = list.end();
    for (int i = 0; i < NUM; ++i) {
        assert(it != end);
        assert(*it == ORIGINAL[i]);
        ++it;
    }

    assert(it == list.end());
    assert(it == end);

    std::cout << "test \'test_iter\'...  passed\n";
    std::cout.flush();
}

void test_assignment(const Unrolled & list)
{
    Unrolled newList;
    newList = list;

    assert(compare_lists(list, newList));

    std::cout << "test \'test_assignment\'...  passed\n";
    std::cout.flush();
}

void test_erasing(const Unrolled & list)
{
    Unrolled tmpList;

    {
        tmpList = list;
        auto it = tmpList.begin();
        auto end = tmpList.end();
        for (int i = 0; i < (NUM - 1); ++i) {
            assert(it != end);
            assert(*it == ORIGINAL[i]);
            it = tmpList.erase(it);
            assert(*it == ORIGINAL[i + 1]);
        }
        it = tmpList.erase(it);
        assert(it == tmpList.end());
        assert(it == end);
    }

    {
        tmpList = list;
        auto it = tmpList.begin();
        auto end = tmpList.end();
        {

        }
    }

    std::cout << "test \'test_erasing\'...  passed\n";
    std::cout.flush();
}

int main()
{
    Unrolled list;

    for (int i = 0; i < NUM; ++i) list.push_back(ORIGINAL[i]);

    test_list(list);
    test_len(list);
    test_iter(list);
    test_assignment(list);
    test_erasing(list);

    return 0;
}
