#if !defined(UNROLLED_LIST_HPP)
#define UNROLLED_LIST_HPP

#include <iterator>

template <typename T, int NODE_LEN>
class unrolled_list
{
private:
	struct node {
		static const int values_size_lb = (NODE_LEN - 2 * sizeof(node*) - sizeof(int)) / sizeof(T);
		static const int values_size = (values_size_lb == 0 ? 1 : values_size_lb);

		node():
			prev(nullptr),
			next(nullptr),
			count(0)
		{
		}
		node * prev;
		node * next;
		int count;
		T val[NODE_LEN];
	};
private:
	node * _head;
	node * _tail;
	int _size;
private:
	void merge_node_with_next(node * node);
public:
	typedef T & reference;
	//typedef const T & const_reference;
public:
	class const_iterator: public std::iterator <
			std::forward_iterator_tag, T, size_t, T const *, T const & >
	{
	public:
		typedef std::bidirectional_iterator_tag iterator_category;
		typedef T value_type;
		typedef ptrdiff_t difference_type;
		typedef T const * pointer;
		typedef T const & reference;
	public:
		const_iterator(node * p_node = nullptr, int index = 0):
			_node(p_node),
			_index(index)
		{
		}

		reference operator*()
		{
			return _node->val[_index];
		}

		pointer operator->()
		{
			return &(**this);
		}

		// prefix ++
		const_iterator & operator++()
		{
			++_index;
			if (_index >= _node->count) {
				if(_node->next != nullptr) {
					_node = _node->next;
					_index = 0;
				}
				else
					_index = _node->count;
			}
			return *this;
		}

		// prefix --
		const_iterator & operator--()
		{
			if (_index > 0)
				--_index;
			else if(_node->prev != nullptr) {
				_node = _node->prev;
				_index = _node->count - 1;
			}
			return *this;
		}

		// postfix ++
		const_iterator operator++(int)
		{
			const_iterator old_it = *this;
			++(*this);
			return old_it;
		}

		// postfix --
		const_iterator operator--(int)
		{
			const_iterator old_it = *this;
			--(*this);
			return old_it;
		}

		bool operator ==(const_iterator const & rhs)
		{
			if(_node == rhs._node) {
				// не важно насколько индекс больше длинны
				// это одно и тоже состояние
				const int count = _node->count;
				const bool a = _index >= count;
				const bool b = rhs._index >= count;
				if(a && b)
					return true;
				else
					return (_index == rhs._index);
			}
			return false;
		}

		bool operator !=(const_iterator const & rhs)
		{
			return !(*this == rhs);
		}
	protected:
		node * _node;
		int _index;
	};

	class iterator: public const_iterator
	{
	public:
		typedef std::bidirectional_iterator_tag iterator_category;
		typedef T      value_type;
		typedef size_t difference_type;
		typedef T   *  pointer;
		typedef T   &  reference;
		using const_iterator::_node;
		using const_iterator::_index;
	public:
		iterator(node * p_node = 0, int index = 0):
			const_iterator(p_node, index)
		{
		}

		reference operator*()
		{
			return _node->val[_index];
		}

		pointer operator ->()
		{
			return &(**this);
		}

		// prefix ++
		iterator & operator++()
		{
			const_iterator::operator++();
			return *this;
		}

		// prefix --
		iterator & operator--()
		{
			const_iterator::operator--();
			return *this;
		}

		// postfix ++
		iterator operator++(int)
		{
			iterator old_it = *this;
			++(*this);
			return old_it;
		}

		// postfix --
		iterator operator--(int)
		{
			iterator old_it = *this;
			--(*this);
			return old_it;
		}
	};

	unrolled_list();
	unrolled_list(unrolled_list<T, NODE_LEN> const & rhs);
	~unrolled_list();
	iterator begin();
	iterator end();
	const_iterator begin() const;
	const_iterator end() const;
	void push_front(T const & elt);
	void push_back(T const & elt);
	iterator insert(iterator where, T const & elt);
	iterator erase(iterator where);
	void pop_front();
	void pop_back();
	void clear();
	unrolled_list & operator =(unrolled_list<T, NODE_LEN> const & rhs);
	T & front();
	T & back();
	const T & front() const;
	const T & back() const;
	size_t size() const;
	bool empty() const;
	friend class unrolled_list<T, NODE_LEN>::iterator;
};

template <typename T, int NODE_LEN>
typename unrolled_list<T, NODE_LEN>::iterator
		unrolled_list<T, NODE_LEN>::begin()
{
	return iterator(_head, 0);
}

template <typename T, int NODE_LEN>
typename unrolled_list<T, NODE_LEN>::iterator
		unrolled_list<T, NODE_LEN>::end()
{
	return iterator(_tail, _tail->count);
}

template <typename T, int NODE_LEN>
typename unrolled_list<T, NODE_LEN>::const_iterator
		unrolled_list<T, NODE_LEN>::begin() const
{
	return const_iterator(_head, 0);
}

template <typename T, int NODE_LEN>
typename unrolled_list<T, NODE_LEN>::const_iterator
		unrolled_list<T, NODE_LEN>::end() const
{
	return const_iterator(_tail, _tail->count);
}

template <typename T, int NODE_LEN>
typename unrolled_list<T, NODE_LEN>::iterator
		unrolled_list<T, NODE_LEN>::insert(
				typename unrolled_list<T, NODE_LEN>::iterator where, T const & elt)
{
	// if node is full
	if (where._node->count == NODE_LEN) {
		node * p_node = new node;
		if (where._index != where._node->count) {
			for(int i = 0; i < (NODE_LEN + 1) / 2; ++i) {
				p_node->val[i] = where._node->val[i + NODE_LEN / 2];
			}
			where._node->count = NODE_LEN / 2;
			p_node->count = (NODE_LEN + 1) / 2;
		}
		p_node->next = where._node->next;
		p_node->prev = where._node;
		if (where._node->next != 0) {
			where._node->next->prev = p_node;
		}
		where._node->next = p_node;
		if (p_node->next == 0) {
			_tail = p_node;
		}
		if (where._index >= where._node->count) {
			where._index -= where._node->count;
			where._node = p_node;
		}
	}
	// shift elements to the right
	for(int i = where._node->count; i > where._index; --i) {
		where._node->val[i] = where._node->val[i - 1];
	}
	// store new element
	where._node->val[where._index] = elt;
	where._node->count++;
	_size++;
	return where;
}

template <typename T, int NODE_LEN>
void unrolled_list<T, NODE_LEN>::push_front(T const & elt)
{
	insert(begin(), elt);
}

template <typename T, int NODE_LEN>
void unrolled_list<T, NODE_LEN>::push_back(T const & elt)
{
	insert(end(), elt);
}

template <typename T, int NODE_LEN>
typename unrolled_list<T, NODE_LEN>::iterator
		unrolled_list<T, NODE_LEN>::erase(
				typename unrolled_list<T, NODE_LEN>::iterator where)
{
    node * p_curr = where._node;
    if(p_curr == nullptr) {
        return where;
    }
    // shift elemnts to the left
    for(int i = where._index; i < p_curr->count - 1; ++i) {
        p_curr->val[i] = p_curr->val[i + 1];
    }
    --(p_curr->count);
    --_size;
    /*const int num_val = unrolled_list<T, NODE_LEN>::node::values_size;
    const int threshold = num_val >= 5 ? num_val * 4 / 5 : num_val - 1;
    if (where._curnode->prev != 0 &&
            (where._curnode->count == 0 ||
             where._curnode->count + where._curnode->prev->count <= threshold)) {
        where._curnode = where._curnode->prev;
        where._curindex += where._curnode->count;
        merge_node_with_next(where._curnode);
    }
    else if (where._curnode->next != 0 &&
                     (where._curnode->count == 0 ||
                        where._curnode->count + where._curnode->next->count <= threshold)) {
        merge_node_with_next(where._curnode);
    }*/

    node * p_prev = p_curr->prev;
    node * p_next = p_curr->next;

    if(p_next != nullptr){
        if (where._index >= p_curr->count) {
            where._index -= p_curr->count;
            where._node = p_next;
        }

        if(p_curr->count == 0) {
            if(p_prev == nullptr)
                _head = p_next;
            else
                p_prev->next = p_next;
            p_next->prev = p_prev;
            delete p_curr;
        }
    }

    return where;
}
template <typename T, int NODE_LEN>
void unrolled_list<T, NODE_LEN>::merge_node_with_next(
		typename unrolled_list<T, NODE_LEN>::node * node)
{
	typename unrolled_list<T, NODE_LEN>::node * next = node->next;
	const int count = node->count;
	for(int i = 0; i < next->count; i++) {
		node->val[i + count] = next->val[i];
	}
	node->count += next->count;
	node->next = node->next->next;
	if (node->next == 0) {
		_tail = node;
	}
	delete next;
}
template <typename T, int NODE_LEN>
void unrolled_list<T, NODE_LEN>::pop_front()
{
	erase(begin());
}

template <typename T, int NODE_LEN>
void unrolled_list<T, NODE_LEN>::pop_back()
{
	erase(--(end()));
}
/* Same effect as:
	 while(begin() != end())
	 {
			 pop_back();
	 }
*/
template <typename T, int NODE_LEN>
void unrolled_list<T, NODE_LEN>::clear()
{
	typename unrolled_list<T, NODE_LEN>::node * n;
	typename unrolled_list<T, NODE_LEN>::node * next;
	for (n = _head->next; n != nullptr; n = next) {
		next = n->next;
		delete n;
	}
	_head->count = 0;
	_head->next = nullptr;
	_tail = _head;
}

template <typename T, int NODE_LEN>
unrolled_list<T, NODE_LEN> &
		unrolled_list<T, NODE_LEN>::operator =(
				unrolled_list<T, NODE_LEN> const & rhs)
{
	clear();
	const_iterator rhs_it = rhs.begin();
	for (rhs_it = rhs.begin(); rhs_it != rhs.end(); ++rhs_it) {
		push_back(*rhs_it);
	}
	return *this;
}

template <typename T, int NODE_LEN>
T & unrolled_list<T, NODE_LEN>::front()
{
	return _head->val[0];
}

template <typename T, int NODE_LEN>
T & unrolled_list<T, NODE_LEN>::back()
{
	return _tail->val[_tail->count - 1];
}

template <typename T, int NODE_LEN>
size_t unrolled_list<T, NODE_LEN>::size() const
{
	return _size;
}

template <typename T, int NODE_LEN>
const T & unrolled_list<T, NODE_LEN>::front() const
{
	return _head->val[0];
}

template <typename T, int NODE_LEN>
const T & unrolled_list<T, NODE_LEN>::back() const
{
	return _tail->val[_tail->count - 1];
}

template <typename T, int NODE_LEN>
bool unrolled_list<T, NODE_LEN>::empty() const
{
	return _size == 0;
}

template <typename T, int NODE_LEN>
unrolled_list<T, NODE_LEN>::unrolled_list():
	_head(new node),
	_tail(_head),
	_size(0)
{
}

template <typename T, int NODE_LEN>
unrolled_list<T, NODE_LEN>::unrolled_list(
		unrolled_list<T, NODE_LEN> const & rhs):
	_head(new node),
	_tail(_head),
	_size(0)
{
	*this = rhs;
}

template <typename T, int NODE_LEN>
unrolled_list<T, NODE_LEN>::~unrolled_list()
{
	clear();
}

#endif
